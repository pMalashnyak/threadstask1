package com.epam.Malashnyak.theadsTask1;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class FibonacciCallable implements Callable<Integer> {

  private int n;
  private List<Integer> num = new ArrayList<Integer>();

  public FibonacciCallable(int _num) {
    n = _num;
    formNum();
    try {
      System.out.println(call());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public Integer call() throws Exception {
    return num.stream().mapToInt(x -> x).sum();
  }

  private Integer addNumber(int number) {
    if (number == 0 || number == 1) {
      return 1;
    }
    return addNumber(number - 1) + addNumber(number - 2);
  }

  public void formNum() {
    for (int i = 0; i < n; ++i) {
      num.add(addNumber(i));
    }
  }
}
