package com.epam.Malashnyak.theadsTask1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Fibonacci extends Thread {
  private List<Integer> num = new ArrayList<Integer>();
  private int n;

  public Fibonacci(int _n) {
    n = _n;
    this.start();
  }

  private Integer addNumber(int number) {
    if (number == 0 || number == 1) {
      return 1;
    }
    return addNumber(number - 1) + addNumber(number - 2);
  }

  @Override
  public void run() {
    System.out.println("Thread of " + n + " fibonacci numbers started");
    long startTime = System.nanoTime();
    for (int i = 0; i < n; ++i) {
      num.add(addNumber(i));
    }
    System.out.println(
        "\nThread with " + n + " numbers ended in " + (System.nanoTime() - startTime));
    num.stream().forEach(System.out::print);
  }

    public List<Integer> getNum() {
        return num;
    }
}
