package com.epam.Malashnyak.theadsTask1;

public class SyncExample {
  private volatile int E;
  private volatile int F;
  private volatile int S;
  private volatile int T;

  public SyncExample() {
    E = 0;
    first();
    second();
    third();
  }

  public SyncExample(int i) {
    F = 0;
    S = 0;
    T = 0;
    fFirst();
    sSecond();
    tThird();
  }

  private void first() {
    Thread thread =
        new Thread(
            () -> {
              System.out.println("First thread started E= " + E);
              for (int i = 0; i < 100; ++i) {
                E++;
              }
              System.out.println("First thread ended with E= " + E);
            });
    thread.start();
  }

  private void second() {
    Thread thread =
        new Thread(
            () -> {
              System.out.println("Second thread started E= " + E);
              for (int i = 0; i < 100; ++i) {
                E++;
              }
              System.out.println("Second thread ended with E= " + E);
            });
    thread.start();
  }

  private void third() {
    Thread thread =
        new Thread(
            () -> {
              System.out.println("Third thread started E= " + E);
              for (int i = 0; i < 100; ++i) {
                E++;
              }
              System.out.println("Third thread ended with E= " + E);
            });
    thread.start();
  }

  private void fFirst() {
    Thread thread =
        new Thread(
            () -> {
              for (int i = 0; i < 10; ++i) {
                F++;
                System.out.println("First thread running.");
              }
            });
    thread.start();
  }

  private void sSecond() {
    Thread thread =
        new Thread(
            () -> {
              for (int i = 0; i < 10; ++i) {
                S++;
                System.out.println("Second thread running.");
              }
            });
    thread.start();
  }

  private void tThird() {
    Thread thread =
        new Thread(
            () -> {
              for (int i = 0; i < 10; ++i) {
                T++;
                System.out.println("Third thread running.");
              }
            });
    thread.start();
  }
}
